package com.springH2Jpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.springH2Jpa.dao.UserRepo;
import com.springH2Jpa.emailService.EmailSenderService;
import com.springH2Jpa.model.UserDetails;

@RestController
public class UserController {

	@Autowired
	EmailSenderService service;

	@Autowired
	UserRepo repo;
	
	@PostMapping("/adduser")
	public List<UserDetails> addUser(@RequestBody UserDetails userDetails) {
		repo.save(userDetails);
		return repo.findAll();
	}

	@GetMapping("/sendemail")
	public void send() {
		
		List<UserDetails> userInfo = repo.findAll(); // TO DO: Find users whose email not send
		System.out.println(userInfo);
		for (UserDetails user : userInfo) {
			String email = user.getEmail();
			
			
//			service.sendsSimpleEmail(email);			

//			try {
//				if (user.getFlag() == false) {
//					service.sendsSimpleEmail(email);
//					System.out.println(user.getFlag());
//					user.setFlag(true);
//					System.out.println(user.getFlag());
//				}
//			} catch (MailException maile) {
//				System.out.println(maile.getMessage());
//			}

//			To do update user flag for send email

		}

	}

	private Object setFlag(boolean b) {
		// TODO Auto-generated method stub
		return null;
	}

//	@RequestMapping("/all")
//	@ResponseBody
//	public String all() {
//		return repo.findAll().toString();
//	}
//
//	@RequestMapping("/all/{email}")
//	@ResponseBody
//	public String getByemail(@PathVariable  String email) {		
//		return repo.findByEmail(email).toString();
//	}

}
