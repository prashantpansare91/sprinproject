package com.springH2Jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringH2JpaApplication {
	
//	@Autowired
//	UserDetails userDetails;
//	
//	@Autowired
//	private EmailSenderService emailSenderService;

	public static void main(String[] args) {
		SpringApplication.run(SpringH2JpaApplication.class, args);
	}
	
//	@EventListener(ApplicationReadyEvent.class)
//	public void triggerMail() {
//		emailSenderService.sendsSimpleEmail(userDetails.getEmail(), "This is the body", "This is subject");
//	}

}
