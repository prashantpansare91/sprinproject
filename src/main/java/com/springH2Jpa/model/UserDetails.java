package com.springH2Jpa.model;

import javax.persistence.Entity;
import javax.persistence.Id;

//@IdClass(UserDetailsPk.class)
@Entity
public class UserDetails {
//	@EmbeddedId
//	private UserDetailsPk userDetailsPk;
//	private String name;
	
	@Id
	private int id;
	private String name;
	private String email;
	boolean flag;
	
	public boolean getFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString() {
		return "id=" + id + ", name=" + name + ", email=" + email +  ", flag =" + flag ;
	}

	

}
