//package com.springH2Jpa.model;
//
//import java.io.Serializable;
//
//import javax.persistence.Embeddable;
//
//import lombok.EqualsAndHashCode;
//
//@EqualsAndHashCode
//@Embeddable
//public class UserDetailsPk implements Serializable{
//	
//	private int id;
//	private String name;
//
//	public int getId() {
//		return id;
//	}
//	public void setId(int id) {
//		this.id = id;
//	}
//	public String getName() {
//		return name;
//	}
//	public void setName(String name) {
//		this.name = name;
//	}
//}
