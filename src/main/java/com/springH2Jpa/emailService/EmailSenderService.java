package com.springH2Jpa.emailService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.springH2Jpa.model.UserDetails;

@Service
public class EmailSenderService {
	
	@Autowired
	private UserDetails userDetails;
	
//	@Autowired
//	private UserRepo repo;
	
	@Autowired
	private JavaMailSender mailSender;
	
	public String sendsSimpleEmail(String toEmail) {
		
		SimpleMailMessage message = new SimpleMailMessage();
		
		message.setFrom("prashantpansare3886@gmail.com");
		message.setTo(toEmail);
		message.setText("Hello in body");
		message.setSubject("im Subject");
	
		
		if(userDetails.getFlag() == false) {
			mailSender.send(message);
			userDetails.setFlag(true);
			System.out.println("mail send......");
		}else {
			System.out.println("not done");
		}
//		mailSender.send(message);
		System.out.println("mail send......");

		return "Done";
	}

		
	}

	


