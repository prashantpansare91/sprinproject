package com.springH2Jpa.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springH2Jpa.model.UserDetails;

@Repository
public interface UserRepo extends JpaRepository<UserDetails, Integer> {

	List<UserDetails> findByEmail(String email);


//	@Query("select email from userDetails")
//	List<String> getByEmail();

}
